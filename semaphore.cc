#include <iostream>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <vector>
#include <unistd.h>


using namespace std;

class Semaphore
{
public :
	Semaphore(int max): max_(max),current_(max){}
	void aquire(std::thread::id tid);
	void release(std::thread::id tid);

private:
	std::mutex mtx_;
	std::condition_variable cv_;
	int max_, current_;

} sem(10);

void Semaphore::aquire(std::thread::id tid)
{
	std::unique_lock<std::mutex> lk(mtx_);
	--current_;
	if (current_ < 0)
	{
		cout << tid << " is waiting.." << endl;
		cv_.wait(lk);
	}
	
	cout << tid << " aquired the semaphore." << endl;
}	


void Semaphore::release(std::thread::id tid)
{
	std::unique_lock<std::mutex> lk(mtx_);
	if (current_ < max_)
		++current_;
	cv_.notify_one();

	cout << tid << " released the semaphore." << endl;
}

void f1()
{
	sem.aquire(std::this_thread::get_id());	
	sleep(2);
	sem.release(std::this_thread::get_id());
}

int main()
{
	std::vector<std::thread*> vec;

	for (int i=0; i < 12; i++)
	{
		vec.push_back(new std::thread(f1));
	}

	for (auto item : vec)
	{
		item->join();
	}

	return 0;
}
